export const initializeBoard = () => {
  return Array.from(Array(9), (_, index) => ({
    id: index + 1,
    type: undefined,
  }));
};

export const SquareType = {
  cross: "CROSS",
  circle: "CIRCLE",
};

export const definePlayerTwoType = (playerOneType) => {
  if (!playerOneType) return undefined;

  return playerOneType === SquareType.cross
    ? SquareType.circle
    : SquareType.cross;
};

export const winningCombo = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
  [2, 5, 8],
  [1, 5, 9],
  [3, 5, 7],
  [1, 4, 7],
  [3, 6, 9],
];

export const playerHasWon = (playerType, squares) => {
  const playerSquares = squares.reduce((acc, square) => {
    if (square.type !== playerType) return acc;

    return [...acc, square.id];
  }, []);

  const playerHasWin = winningCombo.some((combo) => {
    return combo.every((c) => playerSquares.includes(c));
  });

  return playerHasWin;
};

export const hasGameEnded = (playerType, squares, isPlayerOne) => {
  const hasWin = playerHasWon(playerType, squares);
  if (hasWin) return `Player ${isPlayerOne ? 1 : 2} wins !!! 🏆`;

  // tie
  const isBoardFull = squares.every((s) => s.type);
  if (isBoardFull) return "It's a tie 🤷🏻‍♀️ ";

  return undefined;
};
